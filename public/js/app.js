var config = {
  dateFormat: 'DD.MM.',
  dateFormatFull: 'DD.MM.YYYY',
  dateTimeFormat: 'DD.MM.YYYY. hh:mm'
};

angular
  .module('Weekly', ['ngDialog', 'cfp.hotkeys', 'monospaced.elastic', 'angular-ladda'])
  .config(function() {
    console.log('config');
  })
  .controller('AppController', function($scope, Project, ngDialog, $rootScope) {
    $scope.days = [{
      name: 'Monday'
    }, {
      name: 'Tuesday'
    }, {
      name: 'Wednesday'
    }, {
      name: 'Thursday'
    }, {
      name: 'Friday'
    }];

    $scope.updateDays = function() {
      _.each($scope.days, function(day, index) {
        day.date = moment($scope.currentWeek.start, config.dateFormat).add(index, 'days').format(config.dateFormatFull)
      });
    };

    $scope.currentWeek = {
      start: moment().startOf('isoWeek').format(config.dateFormat),
      end: moment().startOf('isoWeek').add(4, 'days').format(config.dateFormat)
    };
    $scope.updateDays();

    $scope.changeWeek = function(weeks) {
      var start = moment($scope.currentWeek.start, config.dateFormat).add(weeks, 'week');
      $scope.currentWeek = {
        start: start.format(config.dateFormat),
        end: start.add(4, 'days').format(config.dateFormat)
      }
      $scope.updateDays();
    };


    $scope.getTasks = function(day, project) {
      if(!project) {
        return [];
      }

      return _.filter(project.tasks, function(task) {
        if(!task) {
          return;
        }
        return task.date === day.date;
      });
    };

    $scope.openTasks = function (day, project) {
      $scope.currentProject = project;
      $scope.currentDay = day;
      ngDialog.open({ 
        template: 'templates/tasks.html',
        scope: $scope
      });
    };

    $scope.openProjectsModal = function () {
      ngDialog.open({ 
        template: 'templates/add-project.html',
        scope: $scope
      });
    };

    function getProjects() {
      Project
        .loadProjects()
        .then(function() {
          $scope.projects = Project.getProjects();
        });
    }

    $rootScope.$on('tasks:reload', function(ev, project) {
      Project.getTasks(project)
        .then(function(tasks) {
          project.tasks = tasks;
          $scope.updateDays();
        });
    });

    $rootScope.$on('tasks:delete', function(ev, project, task) {
      project.tasks = _.filter(project.tasks, function(projectTask) {
        return task.id !== projectTask.id; 
      });
    });

    $rootScope.$on('projects:add', function(ev, project) {
      if(project.id) {
        var oldProject = _.findWhere($scope.projects, {id: project.id});
        oldProject.name = project.name;
      } else {
        $scope.projects.push(project);
      }
    });

    $rootScope.$on('projects:delete', function(ev, project) {
      $scope.projects = _.filter($scope.projects, function(scopeProject) {
        return scopeProject.id !== project.id;
      });
    });

    $rootScope.$on('projects:reload', getProjects);

    getProjects();
  })
  .controller('TasksController', function($scope, hotkeys, Project, $rootScope, $timeout) {
    $scope.newTask = {
      date: $scope.currentDay.date
    };
    $scope.loading = {};

    $scope.saveTask = function(day, project, task, $ev) {
      if(!$ev) {
        $rootScope.$emit('tasks:add', project, task);
        $scope.newTask = {
          date: $scope.currentDay.date
        }
        $scope.loading[task.id ? task.id : 'new'] = true;
        Project.saveTask(project, task)
          .then(function(savedTask) {
            $scope.loading[task.id ? task.id : 'new'] = false;
            $rootScope.$emit('tasks:reload', project);
          });
          return;
      }

      if($ev.charCode === 13 && $ev.shiftKey) {
        return;
      }
      
      if($ev.charCode === 13) {
        $ev.preventDefault();
        $rootScope.$emit('tasks:add', project, task);
        $scope.newTask = {
          date: $scope.currentDay.date
        }
        $scope.loading[task.id ? task.id : 'new'] = true;
        Project.saveTask(project, task)
          .then(function(savedTask) {
            $scope.loading[task.id ? task.id : 'new'] = false;
            $rootScope.$emit('tasks:reload', project);
          });
      }

    };
    
    $scope.deleteTask = function(project, task) {
      $rootScope.$emit('tasks:delete', project, task);
      Project.deleteTask(task);
      $rootScope.$emit('tasks:reload', project);
    };

    hotkeys.bindTo($scope)
      .add ({
        combo: 'return',
        description: 'Save or Add a Task',
        allowIn: ['TEXTAREA'],
        callback: function() {}
      });

  })
  .controller('AddProjectController', function($scope, Project, $rootScope, hotkeys) {
    $scope.newProject = {};
    $scope.loading = {};

    $scope.saveProject = function(project, $ev) {
      if(!$ev) {
        $rootScope.$emit('projects:add', project);
        $scope.newProject = {};
        $scope.loading[project.id ? project.id : 'new'] = true;
        Project.saveProject(project)
          .then(function() {
            $scope.loading[project.id ? project.id : 'new'] = false;
            $rootScope.$emit('projects:reload');
          });
          return;
      }

      if($ev.charCode === 13 && $ev.shiftKey) {
        return;
      }

      if($ev.charCode === 13) {
        $ev.preventDefault();
        $rootScope.$emit('projects:add', project);
        $scope.newProject = {};
        $scope.loading[project.id ? project.id : 'new'] = true;
        Project.saveProject(project)
          .then(function() {
            $scope.loading[project.id ? project.id : 'new'] = false;
            $rootScope.$emit('projects:reload');
          });
      }
    }

    $scope.deleteProject = function(project) {
      Project.deleteProject(project);
      $rootScope.$emit('projects:reload');
      $rootScope.$emit('projects:delete', project);
    };

    hotkeys.bindTo($scope)
      .add ({
        combo: 'return',
        description: 'Save or Add a Task',
        allowIn: ['TEXTAREA'],
        callback: function() {}
      });

  })
  .service('Project', function($http) {
    var projects = [];

    function loadProjects() {
      return $http.get('/projects')
        .then(function(res) {
          projects = res.data;
        });
    }
    
    function getProjects() {
      return projects;
    }

    function saveTask(project, task) {
      task.project_id = project.id;
      var url = '/projects/' + project.id + '/tasks' + (task.id ? '/' + task.id : '');
      return $http[task.id ? 'put': 'post'](url, task);
    }

    function getTasks(project) {
      return $http.get('/projects/' + project.id + '/tasks')
        .then(function(res) {
          return res.data;
        });
    }

    function saveProject(project) {
      var url = '/projects' + (project.id ? '/' + project.id : '');
      return $http[project.id ? 'put': 'post'](url, project);
    }

    function deleteProject(project) {
      return $http.delete('/projects/' + project.id);
    }

    function deleteTask(task) {
      return $http.delete('/projects/' + task.project_id + '/tasks/' + task.id);
    }

    return {
      getProjects: getProjects,
      loadProjects: loadProjects,
      saveTask: saveTask,
      getTasks: getTasks,
      saveProject: saveProject,
      deleteTask: deleteTask,
      deleteProject: deleteProject
    }
  });