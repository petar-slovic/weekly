@extends('app')

@section('content')
<div ng-app="Weekly" ng-controller="AppController">
  <div class="container m-t-lg">
    <div class="row">
      <div class="col-sm-4">
        <div class="info">
          <img src="{{ $user->avatar }}">
          {{ $user->name }}
        </div>
      </div>
      <div class="col-sm-4 date t-a-c">
        <button ng-click="changeWeek(-1)" class="btn btn-warning"><i class="fa fa-chevron-left"></i></button>
        @{{ currentWeek.start }} - @{{ currentWeek.end }}
        <button ng-click="changeWeek(+1)" class="btn btn-warning"><i class="fa fa-chevron-right"></i></button>
      </div>
      <div class="col-sm-4 t-a-r actions">
        <button ng-click="openProjectsModal()" class="btn btn-warning">Manage Projects</button>
        <a ng-href="/print/@{{ days[0].date }}/@{{ days[4].date }}" class="btn btn-warning">Print</a>
        <a href="auth/logut" class="btn btn-warning">Logout</a>
      </div>
    </div>
  </div>

  <div class="container" >
  	<div class="days m-t-lg">
      <div class="day-container" ng-repeat="day in days">
        <div class="day">
          <div class="name">@{{ day.name }} <small class="pull-right">@{{ day.date }}</small></div>
          <div class="project" ng-repeat="project in projects" ng-click="openTasks(day, project)" ng-class="{'no-tasks': !getTasks(day, project).length}">
            <div class="name">@{{ project.name }}</div>
            <div class="info">
              <em>@{{ getTasks(day, project).length }} tasks</em>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
