@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2 t-a-c">
			<a href="/auth/login/google" class="btn btn-login m-t-lg">Login with Google</a>
		</div>
	</div>
</div>
@endsection
