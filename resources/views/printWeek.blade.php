@extends('app')

@section('content')

<div class="container print-tasks">
  <div class="info t-a-c">
    <br>
    <small><strong>{{ $days[0]->format('d.m.') }} - {{ $days[4]->format('d.m.Y') }}</strong></small>
    <br>
    <br>
    <img src="{{ Auth::user()->avatar }}">
    {{ Auth::user()->name }}
  </div>

  @foreach($days as $day)
  <div class="day print" style="page-break-before: auto;">
    <div class="day-title-container">
      <h3>{{ $day->format('l') }} <small>{{ $day->format('d.m.Y') }}</small></h3>
    </div>

    <table class="table table-stripped table-bordered">
      <thead><th><strong>Project</strong></th><th><strong>Tasks</strong></th></thead>
      <tbody>
        @foreach($projects as $project)
          @if($project->getTasksForDay($day->format('d.m.Y'))->count())
          <tr>
            <td class="t-a-c"><h3>{{ $project->name }}</h3></td>
            <td class="tasks-container">
              <ul class="tasks">
                @foreach($project->getTasksForDay($day->format('d.m.Y')) as $task)
                <li>{{ $task['desc'] }}</li>
                @endforeach
              </ul>
            </td>
          </tr>
          @endif
        @endforeach
      </tbody>
    </table>
  @endforeach
</div>

@endsection