<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Third Party Services
	|--------------------------------------------------------------------------
	|
	| This file is for storing the credentials for third party services such
	| as Stripe, Mailgun, Mandrill, and others. This file provides a sane
	| default location for this type of information, allowing packages
	| to have a conventional place to find your various credentials.
	|
	*/

	'mailgun' => [
		'domain' => '',
		'secret' => '',
	],

	'mandrill' => [
		'secret' => '',
	],

	'ses' => [
		'key' => '',
		'secret' => '',
		'region' => 'us-east-1',
	],

	'stripe' => [
		'model'  => 'App\User',
		'key' => '',
		'secret' => '',
	],

  'google' => [
    'client_id' => '395552360932-slfoo89a8hmqinoqglpb00mov4fivcbk.apps.googleusercontent.com',
    'client_secret' => 'I_yrwI5r8sALkfYOE_RXVmP4',
    'redirect' => env('GOOGLE_REDIRECT')
  ],

];
