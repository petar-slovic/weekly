<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::controllers([
  'auth' => 'Auth\AuthController',
  'password' => 'Auth\PasswordController',
]);

Route::get('/', 'Auth\AuthController@loginView');
Route::get('/login/{provider?}', 'Auth\AuthController@login');
Route::get('home', 'HomeController@index');
Route::get('/print/{start}/{end}', 'HomeController@printWeek');

Route::resource('projects', 'ProjectsController');
Route::resource('projects.tasks', 'TasksController');
