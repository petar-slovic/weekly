<?php namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use App\AuthenticateUser;

class AuthController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Registration & Login Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles the registration of new users, as well as the
	| authentication of existing users. By default, this controller uses
	| a simple trait to add these behaviors. Why don't you explore it?
	|
	*/

	use AuthenticatesAndRegistersUsers;

	/**
	 * Create a new authentication controller instance.
	 *
	 * @param  \Illuminate\Contracts\Auth\Guard  $auth
	 * @param  \Illuminate\Contracts\Auth\Registrar  $registrar
	 * @return void
	 */
	public function __construct(Guard $auth, Registrar $registrar)
	{
		$this->auth = $auth;
		$this->registrar = $registrar;

		$this->middleware('guest', ['except' => ['getLogout']]);
	}

  public function getLogin(AuthenticateUser $authenticateUser, Request $request, $provider = null) {
    if(!$provider) {
      return \View::make('auth.login');
    }
    return $authenticateUser->execute($request->all(), $this, $provider);
  }

  public function userHasLoggedIn($user) {
      \Session::flash('message', 'Welcome, ' . $user->username);
      return \Redirect::to('/home');
  }

  public function getLogout() {
    \Auth::logout();
    return \Redirect::to('/');
  }

  public function loginView() {
    return \View::make('auth.login');
  }

}
