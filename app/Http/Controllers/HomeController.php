<?php namespace App\Http\Controllers;

class HomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('home')->with('user', \Auth::user());
	}

  public function printWeek($start, $end) {
    $startDate = \DateTime::createFromFormat('d.m.Y', $start);
    $endDate = \DateTime::createFromFormat('d.m.Y', $end);
    $days = [clone $startDate];
    $tmpDay = $startDate;
    while($tmpDay < $endDate) {
      $tmpDay = $tmpDay->add(\DateInterval::createFromDateString('1 day'));
      $days[] = clone $tmpDay;
    }

    $allProjects = \Auth::user()->projects;
    $projectsFiltered = [];
    $allProjects->each(function($project) use ($startDate, $endDate, &$projectsFiltered) {
      $tasksFiltered = $project->tasks->filter(function($task) use($startDate, $endDate) {
        $taskDate = \DateTime::createFromFormat('d.m.Y', $task->date);
        return  ($taskDate > $startDate) && ($taskDate < $endDate);
      })->sortBy(function($task) {
        return \DateTime::createFromFormat('d.m.Y', $task->date)->format('U');
      });

      $projectsFiltered[] = [
        'project' => $project->toArray(),
        'tasks' => $tasksFiltered->toArray()
      ];
    });
    
    // return \View::make('printWeek')->with('projectData', $projectsFiltered);
    return \View::make('printWeek')->with('projects', $allProjects)->with('days', $days);
  }

}
