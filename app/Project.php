<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'projects';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name', 'user_id'];

  public function tasks() {
    return $this->hasMany('App\Task');
  }

  public function getTasksForDay($day) {
    return $this->tasks->filter(function($task) use ($day) {
      return $task->date == $day;
    });
  }

}
