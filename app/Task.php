<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'tasks';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name', 'user_id', 'date', 'desc'];

  public function project() {
    return $this->belongsTo('App\Project');
  }

}
